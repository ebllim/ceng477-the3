#include <iostream>
#include "parser.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <sstream>
#include <cstring>

#define ROTATE_STEP 0.5
#define MOVE_STEP 0.05

char gRendererInfo[512] = {0};
char gWindowTitle[512] = {0};
bool lightingEnabled = 1;
bool openLights[] = {lightingEnabled, lightingEnabled, lightingEnabled, lightingEnabled, lightingEnabled,
                     lightingEnabled, lightingEnabled, lightingEnabled, lightingEnabled,};
//////-------- Global Variables -------/////////

GLuint gpuVertexBuffer;
GLuint gpuNormalBuffer;
GLuint gpuIndexBuffer;


// Sample usage for reading an XML scene file
parser::Scene scene;
static GLFWwindow *win = NULL;

static void errorCallback(int error, const char *description) {
    fprintf(stderr, "Error: %s\n", description);
}

void prepareCamera() {
    parser::Camera *camera = &(scene.camera);
    camera->up = parser::normalize(camera->up);
    camera->gaze = parser::normalize(camera->gaze);
    camera->u = camera->gaze % camera->up;
    camera->u = parser::normalize(camera->u);
    camera->up = camera->u % camera->gaze;
    camera->up = parser::normalize(camera->up);
}

void setCamera() {
    glViewport(0, 0, scene.camera.image_width, scene.camera.image_height);
/* Set camera position */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(
            scene.camera.position.x, scene.camera.position.y, scene.camera.position.z,
            scene.camera.position.x + (scene.camera.gaze.x * scene.camera.near_distance),
            scene.camera.position.y + (scene.camera.gaze.y * scene.camera.near_distance),
            scene.camera.position.z + (scene.camera.gaze.z * scene.camera.near_distance),
            scene.camera.up.x, scene.camera.up.y, scene.camera.up.z
    );
/* Set projection frustrum */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(
            scene.camera.near_plane.x, scene.camera.near_plane.y, scene.camera.near_plane.z, scene.camera.near_plane.w,
            scene.camera.near_distance, scene.camera.far_distance);
}

void turnOnLights();

void turnOffLights();

void toggleLight(ushort i);

static void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    parser::Mat4f tr;
    parser::Vec3f *up = &scene.camera.up;
    parser::Vec3f *left = &scene.camera.u;
    parser::Vec3f *gaze = &scene.camera.gaze;
    parser::Vec3f *cameraPosition = &scene.camera.position;

    if (action == GLFW_PRESS || action == GLFW_REPEAT ) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, GLFW_TRUE);
                break;
            case GLFW_KEY_0:
//                std::cout << "0 pressed" << std::endl;
                lightingEnabled ^= 1;
                if (lightingEnabled) {
                    turnOnLights();
                } else {
                    turnOffLights();
                }
                break;
            case GLFW_KEY_1:
//                std::cout << "1 pressed" << std::endl;
                toggleLight(1);
                break;
            case GLFW_KEY_2:
//                std::cout << "2 pressed" << std::endl;
                toggleLight(2);
                break;
            case GLFW_KEY_3:
//                std::cout << "3 pressed" << std::endl;
                toggleLight(3);
                break;
            case GLFW_KEY_4:
//                std::cout << "4 pressed" << std::endl;
                toggleLight(4);
                break;
            case GLFW_KEY_5:
//                std::cout << "5 pressed" << std::endl;
                toggleLight(5);
                break;
            case GLFW_KEY_6:
//                std::cout << "6 pressed" << std::endl;
                toggleLight(6);
                break;
            case GLFW_KEY_7:
//                std::cout << "7 pressed" << std::endl;
                toggleLight(7);
                break;
            case GLFW_KEY_8:
//                std::cout << "8 pressed" << std::endl;
                toggleLight(8);
                break;
            case GLFW_KEY_9:
//                std::cout << "9 pressed" << std::endl;
                toggleLight(9);
                break;
            case GLFW_KEY_UP:
            case GLFW_KEY_W:
//                std::cout << "W pressed" << std::endl;

//                glMatrixMode(GL_MODELVIEW);
//                glTranslatef(-gaze->x * MOVE_STEP, -gaze->y * MOVE_STEP, -gaze->z * MOVE_STEP);
                *cameraPosition = (*cameraPosition) + (*gaze * (float) (MOVE_STEP));
                setCamera();
                break;
            case GLFW_KEY_LEFT:
            case GLFW_KEY_A:
//                std::cout << "A pressed" << std::endl;

//                glMatrixMode(GL_MODELVIEW);
//                glRotatef(-ROTATE_STEP, up->x, up->y, up->z);
                tr = parser::rotationMatrix(ROTATE_STEP, up->x, up->y, up->z);
//                *up = tr * (*up);
                *gaze = tr * (*gaze);
                prepareCamera();
                setCamera();
                break;
            case GLFW_KEY_DOWN:
            case GLFW_KEY_S:
//                std::cout << "S pressed" << std::endl;
//                glMatrixMode(GL_MODELVIEW);
//                glTranslatef(gaze->x * MOVE_STEP, gaze->y * MOVE_STEP, gaze->z * MOVE_STEP);
                *cameraPosition = (*cameraPosition) + (*gaze * (float) (-MOVE_STEP));
                setCamera();
                break;
            case GLFW_KEY_RIGHT:
            case GLFW_KEY_D:
//                std::cout << "D pressed" << std::endl;

//                glMatrixMode(GL_MODELVIEW);
//                glRotatef(ROTATE_STEP, up->x, up->y, up->z);
                tr = parser::rotationMatrix(-ROTATE_STEP, up->x, up->y, up->z);
//                *up = tr * (*up);
                *gaze = tr * (*gaze);
                prepareCamera();
                setCamera();

                break;
            case GLFW_KEY_U:
//                std::cout << "U pressed" << std::endl;

//                glMatrixMode(GL_MODELVIEW);
//                glRotatef(-ROTATE_STEP, left->x, left->y, left->z);
                tr = parser::rotationMatrix(ROTATE_STEP, left->x, left->y, left->z);
//                *left = tr * (*left);
                *gaze = tr * (*gaze);
                prepareCamera();
                setCamera();
                break;
            case GLFW_KEY_J:
//                std::cout << "J pressed" << std::endl;

//                glMatrixMode(GL_MODELVIEW);
//                glRotatef(ROTATE_STEP, left->x, left->y, left->z);
                tr = parser::rotationMatrix(-ROTATE_STEP, left->x, left->y, left->z);
//                *left = tr * (*left);
                *gaze = tr * (*gaze);
                prepareCamera();
                setCamera();
                break;
            default:
                break;
        }
    }
}

struct Vertex {
    float x, y, z;        //Vertex
    float nx, ny, nz;     //Normal
    unsigned int normalCount;
};

void normalizeVertexNorm(Vertex &v) {
    auto l = std::sqrt(v.nx * v.nx + v.ny * v.ny + v.nz * v.nz);
    v.nx /= l;
    v.ny /= l;
    v.nz /= l;
}

void calcVertexData(Vertex *pvertex) {
    for (auto &mesh: scene.meshes) {
        for (auto &face: mesh.faces) {
//             here first calculate normal of the face and add it to the vertex
//            n = c -b cross a-b
            auto a = scene.vertex_data[face.v0_id - 1];
            auto b = scene.vertex_data[face.v1_id - 1];
            auto c = scene.vertex_data[face.v2_id - 1];
            auto n = (c - b) % (a - b);
            pvertex[face.v0_id - 1].nx +=
                    (n.x - pvertex[face.v0_id - 1].nx) / (++pvertex[face.v0_id - 1].normalCount);
            pvertex[face.v0_id - 1].ny +=
                    (n.y - pvertex[face.v0_id - 1].ny) / (pvertex[face.v0_id - 1].normalCount);
            pvertex[face.v0_id - 1].nz +=
                    (n.z - pvertex[face.v0_id - 1].nz) / (pvertex[face.v0_id - 1].normalCount);
            normalizeVertexNorm(pvertex[face.v0_id - 1]);

            pvertex[face.v1_id - 1].nx +=
                    (n.x - pvertex[face.v1_id - 1].nx) / (++pvertex[face.v1_id - 1].normalCount);
            pvertex[face.v1_id - 1].ny +=
                    (n.y - pvertex[face.v1_id - 1].ny) / (pvertex[face.v1_id - 1].normalCount);
            pvertex[face.v1_id - 1].nz +=
                    (n.z - pvertex[face.v1_id - 1].nz) / (pvertex[face.v1_id - 1].normalCount);
            normalizeVertexNorm(pvertex[face.v1_id - 1]);

            pvertex[face.v2_id - 1].nx +=
                    (n.x - pvertex[face.v2_id - 1].nx) / (++pvertex[face.v2_id - 1].normalCount);
            pvertex[face.v2_id - 1].ny +=
                    (n.y - pvertex[face.v2_id - 1].ny) / (pvertex[face.v2_id - 1].normalCount);
            pvertex[face.v2_id - 1].nz +=
                    (n.z - pvertex[face.v2_id - 1].nz) / (pvertex[face.v2_id - 1].normalCount);
            normalizeVertexNorm(pvertex[face.v2_id - 1]);
        }
    }
}

void RenderFunction(Vertex *pVertex) {

    Vertex *tmpVertex;
    parser::Vec3f *tmpVec3f;
    parser::Vec4f *tmpVec4f;
    GLfloat ambColor[4] = {0, 0, 0, 1};
    GLfloat diffColor[4] = {0, 0, 0, 1};
    GLfloat specColor[4] = {0, 0, 0, 1};
    GLfloat specExp[1] = {0};
    glShadeModel(GL_SMOOTH);
    for (auto &mesh : scene.meshes) {
        parser::Material *material = &scene.materials[mesh.material_id - 1];
        ambColor[0] = material->ambient.x;
        ambColor[1] = material->ambient.y;
        ambColor[2] = material->ambient.z;
        diffColor[0] = material->diffuse.x;
        diffColor[1] = material->diffuse.y;
        diffColor[2] = material->diffuse.z;
        specColor[0] = material->specular.x;
        specColor[1] = material->specular.y;
        specColor[2] = material->specular.z;
        specExp[0] = material->phong_exponent;
        glMaterialfv(GL_FRONT, GL_AMBIENT, ambColor);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, diffColor);
        glMaterialfv(GL_FRONT, GL_SPECULAR, specColor);
        glMaterialfv(GL_FRONT, GL_SHININESS, specExp);

        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();

//        for (auto &transformation: mesh.transformations) {
        for (int i = mesh.transformations.size() - 1; i >= 0; --i) {
            auto &transformation = mesh.transformations[i];
            if ("Translation" == transformation.transformation_type) {
                tmpVec3f = &scene.translations[transformation.id - 1];
                glTranslatef(tmpVec3f->x, tmpVec3f->y, tmpVec3f->z);
            } else if ("Rotation" == transformation.transformation_type) {
                tmpVec4f = &scene.rotations[transformation.id - 1];
                glRotatef(tmpVec4f->x, tmpVec4f->y, tmpVec4f->z, tmpVec4f->w);
            } else if ("Scale" == transformation.transformation_type) {
                tmpVec3f = &scene.scalings[transformation.id - 1];
                glScalef(tmpVec3f->x, tmpVec3f->y, tmpVec3f->z);
            }
        }

        for (auto &face : mesh.faces) {

            if (mesh.mesh_type == "Solid") {
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            } else {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }

            glBegin(GL_POLYGON);

            tmpVertex = &pVertex[face.v0_id - 1];
            glNormal3f(tmpVertex->nx, tmpVertex->ny, tmpVertex->nz);
            glVertex3f(tmpVertex->x, tmpVertex->y, tmpVertex->z);

            tmpVertex = &pVertex[face.v1_id - 1];
            glNormal3f(tmpVertex->nx, tmpVertex->ny, tmpVertex->nz);
            glVertex3f(tmpVertex->x, tmpVertex->y, tmpVertex->z);

            tmpVertex = &pVertex[face.v2_id - 1];
            glNormal3f(tmpVertex->nx, tmpVertex->ny, tmpVertex->nz);
            glVertex3f(tmpVertex->x, tmpVertex->y, tmpVertex->z);

            glEnd();
        }
        glPopMatrix();
    }


}


void turnOnLights() {
    glEnable(GL_LIGHTING);
    int nlights = scene.point_lights.size();
    for (int i = 0; i < nlights; i++) {
        openLights[i] = 1;
        glEnable(GL_LIGHT0 + i);
        GLfloat col[] = {scene.point_lights[i].intensity.x, scene.point_lights[i].intensity.y,
                         scene.point_lights[i].intensity.z, 1.0f};
        GLfloat pos[] = {scene.point_lights[i].position.x, scene.point_lights[i].position.y,
                         scene.point_lights[i].position.z, 1.0f};
        glLightfv(GL_LIGHT0 + i, GL_POSITION, pos);
        glLightfv(GL_LIGHT0 + i, GL_AMBIENT, reinterpret_cast<const GLfloat *>(&(scene.ambient_light)));
        glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, col);
        glLightfv(GL_LIGHT0 + i, GL_SPECULAR, col);
    }
}

void updateLights(){
    int nlights = scene.point_lights.size();
    for (int i = 0; i < nlights; i++) {
        if(!openLights[i]){
            continue;
        }
        glEnable(GL_LIGHT0 + i);
        GLfloat col[] = {scene.point_lights[i].intensity.x, scene.point_lights[i].intensity.y,
                         scene.point_lights[i].intensity.z, 1.0f};
        GLfloat pos[] = {scene.point_lights[i].position.x, scene.point_lights[i].position.y,
                         scene.point_lights[i].position.z, 1.0f};
        glLightfv(GL_LIGHT0 + i, GL_POSITION, pos);
        glLightfv(GL_LIGHT0 + i, GL_AMBIENT, reinterpret_cast<const GLfloat *>(&(scene.ambient_light)));
        glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, col);
        glLightfv(GL_LIGHT0 + i, GL_SPECULAR, col);
    }
}

void turnOffLights() {
    int nlights = scene.point_lights.size();
//    glDisable(GL_LIGHTING);
    for (int i = 0; i < nlights; i++) {
        glDisable(GL_LIGHT0 + i);
        openLights[i] = 0;
    }
}

void toggleLight(ushort i) {
    openLights[i - 1] ^= 1;
    if (openLights[i - 1]) {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0 + i - 1);
    } else {
        glDisable(GL_LIGHT0 + i - 1);
    }
}

void initVertices(Vertex *pvertex) {
    int i = 0;

    for (auto &vertex : scene.vertex_data) {
        pvertex[i].x = vertex.x;
        pvertex[i].y = vertex.y;
        pvertex[i].z = vertex.z;
        pvertex[i].nx = 0;
        pvertex[i].ny = 0;
        pvertex[i].nz = 0;
        pvertex[i].normalCount = 0;
        i++;
    }
}

int main(int argc, char *argv[]) {
    scene.loadFromXml(argv[1]);

    glfwSetErrorCallback(errorCallback);

    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    win = glfwCreateWindow(scene.camera.image_width, scene.camera.image_height, "CENG477", NULL, NULL);
    if (!win) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(win);

    GLenum err = glewInit();
    if (err != GLEW_OK) {
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(win, keyCallback);

    strcpy(gRendererInfo, (const char *) glGetString(GL_RENDERER));
    strcat(gRendererInfo, " - ");
    strcat(gRendererInfo, (const char *) glGetString(GL_VERSION));

    glEnable(GL_DEPTH_TEST);
    glColor3f(0, 0, 0);

    prepareCamera();
    setCamera();
    turnOnLights();
    Vertex pvertex[scene.vertex_data.size()];

    initVertices(pvertex);
    calcVertexData(pvertex);
    turnOnLights();

    double lastTime = glfwGetTime();
    double currentTime;
    int framesRendered = 0;
    while (!glfwWindowShouldClose(win)) {
//        glfwWaitEvents();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        updateLights();
        RenderFunction(pvertex);

        glfwSwapBuffers(win);
        glfwPollEvents();

        currentTime = glfwGetTime();
        framesRendered++;
        if (currentTime - lastTime >= 1.0) {
            std::stringstream stream;
            stream << framesRendered;
            strcpy(gWindowTitle, gRendererInfo);
            strcat(gWindowTitle, " - ");
            strcat(gWindowTitle, stream.str().c_str());
            strcat(gWindowTitle, " FPS");
            glfwSetWindowTitle(win, gWindowTitle);
            framesRendered = 0;
            lastTime += 1.0;
        }
    }

    glfwDestroyWindow(win);
    glfwTerminate();

    exit(EXIT_SUCCESS);

    return 0;
}
