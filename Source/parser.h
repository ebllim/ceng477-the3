#ifndef __HW1__PARSER__
#define __HW1__PARSER__

#include <string>
#include <vector>
#include <math.h>
#include <cmath>

namespace parser {
    //Notice that all the structures are as simple as possible
    //so that you are not enforced to adopt any style or design.
    struct Vec3f {
        float x, y, z;

        Vec3f() = default;

        explicit Vec3f(float a)
                : x(a), y(a), z(a) {}

        Vec3f(float a, float b, float c)
                : x(a), y(b), z(c) {}

        Vec3f operator-() const {
            return Vec3f(-x, -y, -z);
        }

        Vec3f operator+(const Vec3f &vec) const {
            return Vec3f(x + vec.x, y + vec.y, z + vec.z);
        }

        Vec3f operator-(const Vec3f &vec) const {
            return Vec3f(x - vec.x, y - vec.y, z - vec.z);
        }

        Vec3f operator*(const Vec3f &vec) const {
            return Vec3f(x * vec.x, y * vec.y, z * vec.z);
        }

        Vec3f operator/(const Vec3f &vec) const {
            return Vec3f(x / vec.x, y / vec.y, z / vec.z);
        }

        float operator[](int i) const {
            return (&x)[i];
        }

        Vec3f operator%(const Vec3f &vec2) const {
            return Vec3f(y * vec2.z - z * vec2.y, z * vec2.x - x * vec2.z, x * vec2.y - y * vec2.x);
        }
    };

    static Vec3f operator*(const Vec3f &vec, float scalar) {
        return Vec3f(vec.x * scalar, vec.y * scalar, vec.z * scalar);
    }

    static Vec3f operator*(float scalar, const Vec3f &vec) {
        return Vec3f(vec.x * scalar, vec.y * scalar, vec.z * scalar);
    }

    static Vec3f operator/(const Vec3f &vec, float scalar) {
        return Vec3f(vec.x / scalar, vec.y / scalar, vec.z / scalar);
    }

    static Vec3f operator/(float scalar, const Vec3f &vec) {
        return Vec3f(scalar / vec.x, scalar / vec.y, scalar / vec.z);
    }

    static float length(const Vec3f &vec) {
        return std::sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
    }

    static float dot(const Vec3f &vec1, const Vec3f &vec2) {
        return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
    }

    static Vec3f cross(const Vec3f &vec1, const Vec3f &vec2) {
        return Vec3f(vec1.y * vec2.z - vec1.z * vec2.y, vec1.z * vec2.x - vec1.x * vec2.z,
                     vec1.x * vec2.y - vec1.y * vec2.x);
    }

    static Vec3f normalize(const Vec3f &vec) {
        return vec / length(vec);
    }

//    static Vec3f min(const Vec3f &vec1, const Vec3f &vec2) {
//        return Vec3f(math::min(vec1.x, vec2.x), math::min(vec1.y, vec2.y), math::min(vec1.z, vec2.z));
//    }
//
//    static Vec3f max(const Vec3f &vec1, const Vec3f &vec2) {
//        return Vec3f(math::max(vec1.x, vec2.x), math::max(vec1.y, vec2.y), math::max(vec1.z, vec2.z));
//    }
//
//    static Vec3f clamp(const Vec3f &vec, float min, float max) {
//        return Vec3f(math::min(math::max(vec.x, min), max), math::min(math::max(vec.y, min), max),
//                     math::min(math::max(vec.z, min), max));
//    }

    static Vec3f reflect(const Vec3f &vec, const Vec3f &normal) {
        return vec - normal * dot(normal, vec) * 2.0f;
    }


    struct Vec3i {
        int x, y, z;
    };

    struct Vec4f {
        float x, y, z, w;
    };

    struct Camera {
        Vec3f position;
        Vec3f gaze;
        Vec3f up;
        Vec3f u;
        Vec4f near_plane;
        float near_distance;
        float far_distance;
        int image_width, image_height;
    };

    struct PointLight {
        Vec3f position;
        Vec3f intensity;
        bool status;
    };

    struct Material {
        Vec3f ambient;
        Vec3f diffuse;
        Vec3f specular;
        float phong_exponent;
    };

    struct Transformation {
        std::string transformation_type;
        int id;
    };

    struct Face {
        int v0_id;
        int v1_id;
        int v2_id;
    };

    struct Mesh {
        int material_id;
        std::vector<Face> faces;
        std::vector<Transformation> transformations;
        std::string mesh_type;
    };

    struct Scene {
        //Data
        Vec3i background_color;
        Camera camera;
        Vec3f ambient_light;
        std::vector<PointLight> point_lights;
        std::vector<Material> materials;
        std::vector<Vec3f> vertex_data;
        std::vector<Vec3f> translations;
        std::vector<Vec3f> scalings;
        std::vector<Vec4f> rotations;
        std::vector<Mesh> meshes;

        //Functions
        void loadFromXml(const std::string &filepath);
    };

    class Mat4f {
    public:
        float matrix[4][4];

        Mat4f() {
            for (auto &i : matrix) {
                i[0] = 0;
                i[1] = 0;
                i[2] = 0;
                i[3] = 0;
            }
        }

        Mat4f operator*(const Mat4f &m) const {

            Mat4f a;
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    a.matrix[i][j] =
                            this->matrix[i][0] * m.matrix[0][j] +
                            this->matrix[i][1] * m.matrix[1][j] +
                            this->matrix[i][2] * m.matrix[2][j] +
                            this->matrix[i][3] * m.matrix[3][j];
                }
            }
            return a;
        }

        void toIdentity() {
            for (auto &i : this->matrix) {
                i[0] = 0;
                i[1] = 0;
                i[2] = 0;
                i[3] = 0;
            }
            this->matrix[0][0] = 1;
            this->matrix[1][1] = 1;
            this->matrix[2][2] = 1;
            this->matrix[3][3] = 1;
        }

        Mat4f transpose() {
            Mat4f a;
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    a.matrix[i][j] = this->matrix[j][i];
                }
            }
            return a;
        }

        Vec4f operator*(Vec4f &point) {
            Vec4f result{};
            float *f = &(result.x);
            for (int i = 0; i < 4; ++i) {
                f[i] =
                        this->matrix[i][0] * point.x +
                        this->matrix[i][1] * point.y +
                        this->matrix[i][2] * point.z +
                        this->matrix[i][3] * point.w;
            }
            return result;
        }

        Vec3f operator*(Vec3f &point) {
            Vec4f r{};
            float *f = &(r.x);
            for (int i = 0; i < 4; ++i) {
                f[i] =
                        this->matrix[i][0] * point.x +
                        this->matrix[i][1] * point.y +
                        this->matrix[i][2] * point.z +
                        this->matrix[i][3] * 1;
            }
            return Vec3f{r.x, r.y, r.z};
        }

    };

    Mat4f scalingMatrix(int scaleX, int scaleY, int scaleZ);

    Mat4f translationMatrix(int translateX, int translateY, int translateZ);

    Mat4f rotationMatrix(double angle, float x, float y, float z);

}

#endif